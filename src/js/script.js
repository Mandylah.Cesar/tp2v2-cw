document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDb();
    connexion.requeteTopRated();


});


class MovieDb {

    constructor() {

        this.APIkey = "44359036dc2a4cc786c575106d95844c";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalActeur = 6;

    }

    requeteTopRated() {

        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", this.retourRequeteTopRated.bind(this));

        xhr.open("GET", this.baseURL + "movie/top_rated?page=1&language=" + this.lang + "&api_key=" + this.APIkey);

        xhr.send();

    }

    retourRequeteTopRated(e) {

        let target = e.currentTarget;  //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).results;
            this.afficheTopRated(data);

            //console.log(data[2].title);
        }
    }

    afficheTopRated(data){

        for (var i = 0; i < this.totalFilm; i++){
            console.log(data[i].title);

            let unArticle = document.querySelector(".template").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;

            if (data[i].overview == ""){
                unArticle.querySelector("p").innerText = "Pas de descriptions";
            }
            else{
                unArticle.querySelector("p").innerText = data[i].overview;
                // reduireTexte(unArticle.querySelector("p"), 12, '...');
            }

            var uneImage = unArticle.querySelector("img").setAttribute("src",this.imgPath + "w" + this.largeurAffiche[3] + data[i].poster_path);


            document.querySelector(".liste-films").appendChild(unArticle);


        }
    }




}